Test instructions
=================

Build: `docker build -t website .`

Run: `docker run -d -p 81:80 --name website website`

Use this container in the haproxy.cfg file
