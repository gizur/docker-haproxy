HA proxy for HTTP traffic
=========================

1. Configure the backends in `haproxy.cfg` (copy the template). The rows looks
like this: `server SERVER_NAME IP:PORT check`. `SERVER_NAME` is only used for
logging purposes.

2. The stats logins also needs to be updated in haproxy.cfg, see
`stats auth admin:password`.

3. Build: `docker build --rm -t haproxy .`

4. Run:

    docker run -t -i -p 80:80 -p 443:443 --restart="on-failure:10" \
    --link beservices:beservices --name=haproxy -h haproxy \
    haproxy /bin/bash -c "supervisord; bash"

and then `/start.sh &`. Disconnect from the shell with `ctrl-p` `ctrl-q`  
(you can connect later with docker attach haprox)

5. Check the logs: `docker logs lb`

6. Check the stats at `http://[IP]/haproxy?stats`

The amount of logging is controlled by: `log 127.0.0.1 local0 debug` in
`haproxy.cfg`


Update configuration
--------------------


It is possible to update the configuration of haproxy while the container is running.

1. Connect to the container with: `docker attach haproxy`
2. Edit config file: `nano /etc/haproxy/haproxy.cfg`
3. Restart haproxy: `supervisorctl restart haproxy`
