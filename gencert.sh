#!/bin/bash
#
# Generate a self signed certificate
#


# Self-signed certificate for development laptops
# -----------------------------------------------

cat > csr.conf << EOF
SE
Gothenburg
Gothenburg
Example Inc.
Example Inc.
gizur.local
name at example dot com


EOF

# Everything in one line
openssl req -nodes -new -x509 -keyout server.key -out server.cer < csr.conf

cat server.cer server.key > /etc/ssl/private/gizur.local.pem



# Self-signed certificate for development server (with public DNS mapping)
# -------------------------------------------------------------------------

cat > csr.conf << EOF
SE
Gothenburg
Gothenburg
Example Inc.
Example Inc.
*.gizur.com
name at example dot com


EOF

# Everything in one line
openssl req -nodes -new -x509 -keyout server.key -out server.cer < csr.conf

cat server.cer server.key > /etc/ssl/private/star.gizur.com.pem
